var app = angular.module('Security',['ui.router']);

app.config(function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/act');
    $stateProvider.state("/", {
        url:"/",
        templateUrl: "greeting.html",
        controller: "MainCtrl"
    }).state("/login", {
        url:"/login",
        templateUrl: "login.html",
        controller: "MainCtrl"
    }).state("/insecure", {
        url:"/insecure",
        templateUrl: "insecure.html",
        controller: "MainCtrl"
    }).state("/secure", {
        url:"/secure",
        templateUrl: "secure.html",
        controller: "MainCtrl"
    });
});
app.controller('MainCtrl', function($scope, $location, $state, $http){
    $scope.navLogin =  function() {$state.go("/login");}
    $scope.navSecure =  function() {$state.go("/secure");}
    $scope.navInsecure =  function() {$state.go("/insecure");}
    $scope.a=  function() {
        $http.get("/api/a").success(
                function(response){
                    console.log(response);
                });
    }
    $scope.b=  function() {
        $http.get("/api/b").success(
                function(response){
                    console.log(response);
                });
    }
    $scope.auth = function() {
        $http({
            method:'POST',
            url:'/login',
            params :{
                password : $scope.pass,
                username : $scope.login
            }
        })
    }
})

