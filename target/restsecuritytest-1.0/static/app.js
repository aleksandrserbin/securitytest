var app = angular.module('Security', ['ui.router']);

app.value('SecurityPathConfig', {
    basePath: "/restsecuritytest"
});

app.config(function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/act');
    $stateProvider.state("/", {
        url: "/",
        templateUrl: "static/greeting.html",
        controller: "MainCtrl"
    }).state("/login", {
        url: "/login",
        templateUrl: "static/login.html",
        controller: "MainCtrl"
    }).state("/insecure", {
        url: "/insecure",
        templateUrl: "static/insecure.html",
        controller: "MainCtrl"
    }).state("/secure", {
        url: "/secure",
        templateUrl: "static/secure.html",
        controller: "MainCtrl"
    }).state("/err", {
        url: "/err",
        templateUrl: "static/err.html",
        controller: "MainCtrl"
    });
});
app.controller('MainCtrl', function ($scope, $location, $state, $http, $rootScope) {
    $scope.navLogin = function () {
        $state.go("/login");
    }
    $scope.navSecure = function () {
        $state.go("/secure");
    }
    $scope.navInsecure = function () {
        $state.go("/insecure");
    }
    $scope.a = function () {
        $http.get("api/insecure").success(
                function (response) {
                    console.log(response);

                });
    }
    $scope.b = function () {
        $http.get("api/secure").success(
                function (response) {
                    console.log(response);
                });
    }
    $scope.auth = function () {
        var username = $scope.login;
        var pass = $scope.pass;
        var headers = {
            authorization: "Basic "
                    + btoa(username + ":"
                            + pass)
        }
//        $http({
//            method: 'POST',
//            url: 'auth',
//            params: {
//                password: $scope.pass,
//                username: $scope.login
//            }
//        }).success(function (data) {
//            if (data.name) {
//                $rootScope.authenticated = true;
//            } else {
//                $rootScope.authenticated = false;
//            }
//        }).error(function () {
//            $rootScope.authenticated = false;
//        });
        $http.get('api/auth', {
            headers: headers
        }).success(function (data) {
            if (data.name) {
                $rootScope.authenticated = true;
                console.log("Login succeeded");
            } else {
                $rootScope.authenticated = false;
                console.log("fuck");
            }
            $state.go("/");
        }).error(function () {
            $rootScope.authenticated = false;
            $state.go("/");
        });
    }


});

