/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.alserb.restsecuritytest;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

/**
 *
 * @author alex
 */
public class UnauthorizedEntryPoint implements AuthenticationEntryPoint{

    @Override
    public void commence(HttpServletRequest resquest,
            HttpServletResponse response, AuthenticationException ae)
            throws IOException, ServletException {
        
        response.sendRedirect("401");
    }
    
}
