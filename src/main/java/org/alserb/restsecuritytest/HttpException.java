
package org.alserb.restsecuritytest;


public class HttpException {
    short status;
    String message;

    public HttpException() {
    }

    public HttpException(short status, String message) {
        this.status = status;
        this.message = message;
    }

    public short getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public void setStatus(short status) {
        this.status = status;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    
}
