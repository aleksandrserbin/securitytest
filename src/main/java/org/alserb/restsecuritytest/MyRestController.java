/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.alserb.restsecuritytest;

import java.security.Principal;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class MyRestController {
    @RequestMapping(value = "insecure", method = RequestMethod.GET)
    public Entity getInsecuredEntity(){
        Entity e =  new Entity();
        e.setId(0);
        e.setName("insecure");
        return e;
    }
    @RequestMapping(value = "secure", method = RequestMethod.GET)
    public Entity getSecuredEntity(){
        Entity e =  new Entity();
        e.setId(0);
        e.setName("secure");
        return e;
    }
    
    @RequestMapping(value="401",method=RequestMethod.GET)
    public HttpException unauthorized(){
        return new HttpException((short)401, "Unauthorized");
        
    }
    
    @RequestMapping(value = "auth", method = RequestMethod.POST)
    public Entity loginPage() {
        Entity e =  new Entity();
        e.setId(1);
        e.setName("authed");
        return e;
    }
    @RequestMapping(value = "auth", method = RequestMethod.GET)
    public Principal user(Principal user) {
        
        //System.out.println(user.getName());
        return user;
    }
}
